import java.util.*;
public class Dosen extends Manusia{
    private String nip;
    private ArrayList<MataKuliah> daftarMatkul = new ArrayList<>();

    public Dosen(){}

    public Dosen(String name, String nip){
        setNama(name);
        this.nip = nip;
    }
    public void setNip(String nip){
        this.nip = nip;
    }
    public String getNip(){
        return nip;
    }
    public void assignMatkul(MataKuliah matkul){
        daftarMatkul.add(matkul);
    }
    @Override
    public String toString(){
      return getNama() + " " + getNip();
    }
}
