public abstract class GenericMatrix<E extends Object> {
    protected abstract E add(E o1, E o2);

    protected abstract E multiply(E o1, E o2);

    protected abstract E zero();

    public static boolean dimenMatrix(Number[][] matrix1, Number[][] matrix2){
        if(matrix1.length == matrix2.length && matrix1[0].length == matrix2[0].length){
            return true;
        }else{
            return false;
        }
    }
    public static boolean isBujurSangkar(Number[][] m){
        if(m.length == m[0].length){
            return true;
        }else{
            return false;
        }
    }
    public static void diagonalPrimer(Number[][] m){
        if(isBujurSangkar(m) == true){
            System.out.print("The component of diagonal prime of m1 is ");
            for(int i=0; i<m.length; i++){
                for(int j=0; j<m[i].length; j++){
                    if(i == j){
                        System.out.print(m[i][j] + " ");
                    }
                }
            }
        }else{
            System.out.println("The matrix doesn`t have diagonal prime");
        }
    }
    public static void segitigaAtas(Number[][] m){
        if(isBujurSangkar(m) == true){
            System.out.print("\nThe matrix triangle up of m1 is ");
            for(int i=0; i<m.length; i++){
                for(int j=0; j<m[i].length; j++){
                    if(i > 0 && j < i){
                        m[i][j] = 0;
                    }
                }
            }
            for(Number[] isi : m){
                System.out.println(" ");
                for(Number x : isi){
                    System.out.print(" " + x);
                }
            }
        }else{
            System.out.println("The matrix m1 doesn`t bujur sangkar");
        }
    }
    public static void inversMatrix(Number[][] m){
        Number[][] n = new Number[2][2];
        for(int i=0; i<m.length;i++){
            for(int j=0; j<m[i].length; j++){
                if(i==0 && j==0){

                }
            }
        }
    }

    public E[][] addMatrix(E[][] matrix1, E[][] matrix2){
        if((matrix1.length != matrix2.length) || (matrix1[0].length != matrix2[0].length)){
            throw new RuntimeException("The matrices do not have the same size");
        }
        try{
            E[][] result = (E[][])new Number[matrix1.length][matrix1[0].length];
            for (int i = 0; i < result.length; i++){
                for (int j = 0; j < result[i].length; j++){
                    result[i][j] = add(matrix1[i][j], matrix2[i][j]);
                }
            }
            return result;
        }catch(ArrayStoreException Exception){
            E[][] result = (E[][])new String[matrix1.length][matrix1[0].length];
            for (int i = 0; i < result.length; i++){
                for (int j = 0; j < result[i].length; j++){
                    result[i][j] = add(matrix1[i][j], matrix2[i][j]);
                }
            }
            return result;
        }
    }
    public E[][] multiplyMatrix(E[][] matrix1, E[][] matrix2) {
        if (matrix1[0].length != matrix2.length) {
          throw new RuntimeException("The matrices do not have compatible size");
        }
        E[][] result = (E[][])new Number[matrix1.length][matrix2[0].length];
    
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = zero();
    
                for (int k = 0; k < matrix1[0].length; k++) {
                    result[i][j] = add(result[i][j], multiply(matrix1[i][k], matrix2[k][j]));
                }
            }
        }
        return result;
    }
    public static void printResult(Number[][] m1, Number[][] m2, Number[][] m3, char op) {
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[i].length; j++)
                System.out.print(" " + m1[i][j]);

            if (i == m1.length / 2)
                System.out.print("  " + op + "  ");
            else
                System.out.print("     ");

            for (int j = 0; j < m2.length; j++)
                System.out.print(" " + m2[i][j]);

            if (i == m1.length / 2)
                System.out.print("  =  ");
            else
                System.out.print("     ");

            for (int j = 0; j < m3.length; j++)
                System.out.print(m3[i][j] + " ");

            System.out.println();
        }
    }
    public static void printResult2(String[][] m1, String[][] m2, String[][] m3, char op){
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[i].length; j++)
                if(m1[i][j].equals("-") == true)
                    System.out.print("  -  ");
                else
                    System.out.print(" " + m1[i][j]);

            if (i == m1.length / 2)
                System.out.print("           " + op + "            ");
            else
                System.out.print("                    ");

            for (int j = 0; j < m2[i].length; j++)
                if(m2[i][j].equals("-") == true)
                    System.out.print("  -  ");
                else
                    System.out.print(" " + m2[i][j]);
                

            if (i == m1.length / 2)
                System.out.print("           =          ");
            else
                System.out.print("                      ");

            for (int j = 0; j < m3[i].length; j++)
                if(m3[i][j].equals("-") == true)
                    System.out.print("  -  ");
                else
                    System.out.print(" " + m3[i][j]);

            System.out.println();
        }
    }
}
