public class Pegawai extends Manusia implements Makhluk{
    private String levelKeahlian;

    public Pegawai(String nama, int uang, String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
        setNama(nama);
        setUang(uang);
    }
    public Pegawai(String nama, int uang, int umur, String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
        setNama(nama);
        setUang(uang);
        setUmur(umur);
    }
    public void setLevelKeahlian(String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
    }
    public String getLevelKeahlian(){
        return levelKeahlian;
    }
    public String bekerja(){
        return getNama() + " bekerja di kedai VoidMain.";
    }
    @Override
    public String bergerak(){
        return getNama() + " bergerak dengan cara berjalan.";
    }
    public String bicara(){
        return "Halo, saya "+getNama()+". Uang saya adalah "+getUang()+", dan level keahlian saya adalah "+getLevelKeahlian()+".";
    }
    public String bernafas(){
        return getNama() + " bernafas dengan paru-paru.";
    }
    public String toString(){
        return bicara();
    }
    
}
