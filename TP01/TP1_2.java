import java.util.Scanner;

public class TP1_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Selamat datang di DDP2");
        System.out.print("Nama fakultas apa yang anda ingin ketahui?: ");
        String singkatan = input.nextLine().toLowerCase();
        
        switch (singkatan) {
            case "fk":
                System.out.println("Fakultas Kedokteran");
                break;
            case "fkg":
                System.out.println("Fakultas Kedokteran Gigi");
                break;
            case "fkm":
                System.out.println("Fakultas Kesehatan Masyarakat");
                break;
            case "fik":
                System.out.println("Fakultas Ilmu Keperawatan");
                break;
            case "ff":
                System.out.println("Fakultas Farmasi");
                break;
            case "fmipa":
                System.out.println("Fakultas Matematika dan Ilmu Pengetahuan Alam");
                break;
            case "ft":
                System.out.println("Fakultas Teknik");
                break;
            case "fasilkom":
                System.out.println("Fakultas Ilmu Komputer");
                break;
            case "fh":
                System.out.println("Fakultas Hukum");
                break;
            case "feb":
                System.out.println("Fakultas Ekonomi dan Bisnis");
                break;
            case "fib":
                System.out.println("Fakultas Ilmu Pengetahuan Budaya");
                break;
            case "fpsi":
                System.out.println("Fakultas Psikologi");
                break;
            case "fisip":
                System.out.println("Fakultas Ilmu Sosial dan Ilmu Politik");
                break;
            case "fia":
                System.out.println("fakultas Ilmu Administrasi");
                break;
            default:
                System.out.println("Fakultas tidak ditemukan");
                break;
        }
    }
}
