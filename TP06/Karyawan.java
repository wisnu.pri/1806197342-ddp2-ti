public class Karyawan {
    private String nama;
    private int umur;
    private int lamaBekerja;
    private double gaji = 100;

    public Karyawan(){
    }
    public Karyawan(String nama, int umur, int lamaBekerja) {
        this.nama = nama;
        this.umur = umur;
        this.lamaBekerja = lamaBekerja;
        setGaji();
    }
    public double getGaji() {
        return gaji;
    }
    public void setGaji() {
        for(int i = 0; i < lamaBekerja/3; i++) {
            this.gaji = gaji*105/100;
        }
        if (umur > 40) {
            this.gaji = gaji + 10;
        }
    }
    public String getNama() {
        return nama;
    }
}
