import java.util.*;
public class Main{
    public static void main(String[] args){
        Scanner inp = new Scanner(System.in);
        String data = "";
        ArrayList<Barang> objElektronik = new ArrayList<Barang>();
        ArrayList<Barang> objPakaian = new ArrayList<Barang>();
        ArrayList<Barang> objCampuran = new ArrayList<Barang>();
        boolean cekJumlahBarang = false;
        boolean formatElektronik = false;
        boolean formatPakaian = false;
        boolean formatCampuran = false;
        int nElektronik = 0;
        int nPakaian = 0;
        int nCampuran = 0;
        System.out.println("Selamat datang di layanan donasi Desa Dedepe Dua!");
        System.out.println("Layanan disponsori oleh Kedai VoidMain");
        while(cekJumlahBarang == false){
            try{
                System.out.println("Ada berapa jumlah barang untuk elektronik, pakaian, dan campuran? (contoh: 1 2 3):");
                String jumlahBarang = inp.nextLine();
                if(jumlahBarang.split(" ").length == 3){
                    nElektronik = Integer.parseInt(jumlahBarang.split(" ")[0]);
                    nPakaian = Integer.parseInt(jumlahBarang.split(" ")[1]);
                    nCampuran = Integer.parseInt(jumlahBarang.split(" ")[2]);
                    cekJumlahBarang = true;
                }else{
                    System.out.println("Input Anda Salah!!");
                }
            }catch(Exception e){
                System.out.println("Input Anda Salah!!");
            }
        }

        for(int i=0; i<nElektronik; i++){
            while(formatElektronik == false){
                try{
                    System.out.println("Silahkan masukan keterangan barang elektronik");
                    System.out.println("Dengan format 'jenis kondisi' tanpa tanda '");
                    data = inp.nextLine();
                    if(data.split(" ").length == 2){
                        Elektronik barangElektronik = new Elektronik(data.split(" ")[0],data.split(" ")[1]);
                        if(barangElektronik.getValue() >0){
                            objElektronik.add(barangElektronik);
                            formatElektronik = true;
                        }else{
                            System.out.println("Input Anda Salah!!");
                            System.out.println("Pastikan barang anda laptop, hp, atau modem dan kondisinya baru,baik,menengah atau buruk");
                        }
                    }else{
                        System.out.println("Input Anda Salah!!");
                    }
                }catch(Exception e){
                    System.out.println("Input Anda Salah!!");
                }
            }
        }
        for(int i=0; i<nPakaian; i++){
            while(formatPakaian == false){
                try{
                    System.out.println("Silahkan masukan keterangan pakaian");
                    System.out.println("Dengan format 'ukuran warna' tanpa tanda '");
                    data = inp.nextLine();
                    if(data.split(" ").length == 2){
                        Pakaian barangPakaian = new Pakaian(data.split(" ")[0].charAt(0),data.split(" ")[1]);
                        if(barangPakaian.getValue() >0){
                            objPakaian.add(barangPakaian);
                            formatPakaian = true;
                        }else{
                            System.out.println("Input Anda Salah!!");
                            System.out.println("Pastikan ukuran pakaian anda L,M,atau S");
                        }
                    }else{
                        System.out.println("Input Anda Salah!!");
                    }
                }catch(Exception e){
                    System.out.println("Input Anda Salah!!");
                }
            }
        }
        for(int i=0; i<nCampuran; i++){
            while(formatCampuran == false){
                try{
                    System.out.println("Silahkan masukan keterangan barang campuran");
                    System.out.println("Dengan format 'ELEKTRONIK jenis kondisi' tanpa tanda ' ");
                    System.out.println("atau 'PAKAIAN ukuran warna' untuk pakaian");
                    data = inp.nextLine().toLowerCase();
                    if(data.split(" ").length == 3){
                        if(data.split(" ")[0].equals("elektronik")){
                            Elektronik barangElektronik = new Elektronik(data.split(" ")[1],data.split(" ")[2]);
                            if(barangElektronik.getValue() >0){
                                objCampuran.add(barangElektronik);
                                formatCampuran = true;
                            }else{
                                System.out.println("Input Anda Salah!!");
                                System.out.println("Pastikan barang anda laptop, hp, atau modem dan kondisinya baru,baik,menengah atau buruk");
                            }
                        }else{
                            Pakaian barangPakaian = new Pakaian(data.split(" ")[1].charAt(0),data.split(" ")[2]);
                            if(barangPakaian.getValue() >0){
                                objCampuran.add(barangPakaian);
                                formatCampuran = true;
                            }else{
                                System.out.println("Input Anda Salah!!");
                                System.out.println("Pastikan ukuran pakaian anda L,M,atau S");
                            }
                        }
                    }else{
                        System.out.println("Input Anda Salah!!");
                    }
                }catch(Exception e){

                }
            }
        }
        inp.close();
        Kardus<Barang> kardusElektronik = new Kardus<Barang>(objectElektronik);
        Kardus<Barang> kardusPakaian = new Kardus<Barang>(objectPakaian);
        Kardus<Barang> kardusCampuran = new Kardus<Barang>(objectCampuran);
        double donasi = kardusElektronik.getTotalValue()+kardusPakaian.getTotalValue()+kardusCampuran.getTotalValue();
        System.out.println("-----------------------------------");
        System.out.println("Terima kasih atas donasi anda.");
        System.out.println("Donasi anda sebesar "+donasi+" DDD");
        System.out.println("Rekap donasi untuk tiap kardus:");
        System.out.println(kardusElektronik.rekap());
        System.out.println(kardusPakaian.rekap());
        System.out.println(kardusCampuran.rekap());
        System.out.println("-----------------------------------");
        ArrayList<ArrayList<Barang>> barangDonasi = new ArrayList<ArrayList<Barang>>();
        barangDonasi.add(kardusElektronik.getListBarang());
        barangDonasi.add(kardusPakaian.getListBarang());
        barangDonasi.add(kardusCampuran.getListBarang());
        TabelPengiriman<Barang> tabel = new TabelPengiriman<Barang>(barangDonasi);
        System.out.println(tabel.rekap());

    }
}

