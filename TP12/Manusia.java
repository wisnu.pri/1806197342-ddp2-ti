public abstract class Manusia {
    private String name;
    
    protected Manusia(){}
    
    protected Manusia(String name){
        this.name = name;
    }
    public void setNama(String name){
        this.name = name;
    }
    public String getNama(){
        return name;
    }
    public abstract String toString();
}
