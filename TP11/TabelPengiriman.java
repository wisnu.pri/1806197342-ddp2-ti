import java.util.*;
public class TabelPengiriman<T extends Barang> {
    private ArrayList<ArrayList<T>> isiTabel;

    public TabelPengiriman(ArrayList<ArrayList<T>> isiTabel){
        this.isiTabel = isiTabel;
    }
    public String rekap(){
        ArrayList<Kardus> kardus = new ArrayList<Kardus>();
        Object[] kolomElektronik = new Object[4];
        Object[] kolomPakaian = new Object[4];
        Object[] kolomCampuran = new Object[4];
        String keluaranElektronik = "Sierra: ";
        String keluaranPakaian = "Delta: ";
        String keluaranCampuran = "Alpha: ";
        for(int i = 0; i <isiTabel.size(); i++){
            Kardus data = new Kardus(this.isiTabel.get(i));
            kardus.add(data);
        }
        for(int i = 0; i <kardus.size(); i++){
            for(int j=0; j<kardus.get(i).getListBarang().size(); j++){
                if(i==0){
                    if(kardus.get(i).getListBarang().get(j).toString().split(" ")[1].equals("Baru")){
                        kolomElektronik[0]=kardus.get(i).getListBarang().get(j);
                    }else if(kardus.get(i).getListBarang().get(j).toString().split(" ")[1].equals("Baik")){
                        kolomElektronik[1] = kardus.get(i).getListBarang().get(j);
                    }else if(kardus.get(i).getListBarang().get(j).toString().split(" ")[1].equals("Menengah")){
                        kolomElektronik[2] = kardus.get(i).getListBarang().get(j);
                    }else{
                        kolomElektronik[3] = kardus.get(i).getListBarang().get(j);
                    }
                }else if(i==1){
                    if(kardus.get(i).getListBarang().get(j).toString().split(" ")[0].equals("L")){
                        kolomPakaian[0]= kardus.get(i).getListBarang().get(j);
                    }else if(kardus.get(i).getListBarang().get(j).toString().split(" ")[0].equals("M")){
                        kolomPakaian[1]= kardus.get(i).getListBarang().get(j);
                    }else{
                        kolomPakaian[2]= kardus.get(i).getListBarang().get(j);
                    }
                }else{
                    if(kardus.get(i).getListBarang().get(j).toString().split(" ")[1].equals("Baru")){
                        kolomCampuran[0] = kardus.get(i).getListBarang().get(j);
                    }else if(kardus.get(i).getListBarang().get(j).toString().split(" ")[1].equals("Baik")){
                        kolomCampuran[1]= kardus.get(i).getListBarang().get(j);
                    }else if(kardus.get(i).getListBarang().get(j).toString().split(" ")[1].equals("Menengah")){
                        kolomCampuran[2]= kardus.get(i).getListBarang().get(j);
                    }else if(kardus.get(i).getListBarang().get(j).toString().split(" ")[0].equals("L")){
                        kolomCampuran[0]= kardus.get(i).getListBarang().get(j);
                    }else if(kardus.get(i).getListBarang().get(j).toString().split(" ")[0].equals("M")){
                        kolomCampuran[1]= kardus.get(i).getListBarang().get(j);
                    }else if(kardus.get(i).getListBarang().get(j).toString().split(" ")[0].equals("S")){
                        kolomCampuran[2]= kardus.get(i).getListBarang().get(j);
                    }else{
                        kolomCampuran[3]= kardus.get(i).getListBarang().get(j);
                    }
                }
            }
            if(i==0){
                for(int j=0; j<kolomElektronik.length; j++){
                    try{
                        keluaranElektronik += kolomElektronik[j].toString()+"/";
                    }catch(NullPointerException e){
                        keluaranElektronik += "None/";
                    }
                }
                keluaranElektronik+= "\n";
            }else if(i==1){
                for(int j=0; j<kolomPakaian.length; j++){
                    try{ 
                        keluaranPakaian += kolomPakaian[j].toString()+"/";
                    }catch(NullPointerException e){
                        keluaranPakaian += "None/";
                    }
                }
                keluaranPakaian+="\n";
            }else{
                for(int j=0; j<kolomPakaian.length; j++){
                    try{
                        keluaranCampuran += kolomCampuran[j].toString()+"/";
                    }catch(NullPointerException e){
                        keluaranCampuran += "None/";
                    }
                }
                keluaranCampuran+="\n";
            }
        }
    return "Rekap Asuransi Mitra:\n"+keluaranElektronik+keluaranPakaian+keluaranCampuran;
    }

}
