public interface Makhluk {
    public abstract String bernafas();

    public abstract String bergerak();

    public abstract String bisaLibur();

    public abstract String bisaTerbang();
}
