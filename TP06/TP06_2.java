import java.util.Scanner;

public class TP06_2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int value = sc.nextInt();
        Karyawan [] listKaryawan = new Karyawan[value];
        for (int i = 0; i < value; i++){
            String nama = sc.next();
            int umur = sc.nextInt();
            int lamaBekerja = sc.nextInt();
            Karyawan data = new Karyawan(nama, umur, lamaBekerja);
            listKaryawan[i] = data; 
        }
        System.out.printf("Rata-rata gaji karyawan adalah %.2f%n", rerataGaji(listKaryawan));
        System.out.println("Karyawan dengan gaji tertinggi adalah " + gajiTertinggi(listKaryawan));
        System.out.println("Karyawan dengan gaji terendah adalah " + gajiTerendah(listKaryawan));
    }
    public static double rerataGaji(Karyawan[] listKaryawan) {
        double total = 0;
        for (int i = 0; i < listKaryawan.length; i++) {
            total += listKaryawan[i].getGaji();
        }return total/listKaryawan.length;
    }
    public static String gajiTerendah(Karyawan[] listKaryawan) {
        Karyawan lowGaji = listKaryawan[0];
        for (int i = 1; i < listKaryawan.length; i++){
            Karyawan cekGaji = listKaryawan[i];
            if (cekGaji.getGaji() < lowGaji.getGaji()){
                lowGaji = cekGaji;
            }
        }return lowGaji.getNama();
    }
    public static String gajiTertinggi(Karyawan[] listKaryawan) {
        Karyawan lowGaji = listKaryawan[0];
        for (int i = 1; i < listKaryawan.length; i++){
            Karyawan cekGaji = listKaryawan[i];
            if (cekGaji.getGaji() > lowGaji.getGaji()){
                lowGaji = cekGaji;
            }
        }return lowGaji.getNama();
    }
}
