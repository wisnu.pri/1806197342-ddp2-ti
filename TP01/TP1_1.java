import java.util.Scanner;     // Untuk menerima input
import Math;                  // Mengolah perhitungan

public class TP1_1 {
    public static void main(String[] args) {
        System.out.println("Selamat datang di DDP2");
        Scanner input = new Scanner(System.in);
        // Antisipasi error
        try {
            // Terima input kata
            System.out.print("Masukkan kata: ");
            String kata = input.nextLine(); int length = kata.length();
            // Terima input angka
            System.out.print("Masukkan angka: ");
            int angka = input.nextInt();
            // Percabangan cek angka
            if (Math.pow(angka, 2) == 4.0) {
                System.out.println("Angka bernilai 2");
            } else if (angka % 2 == 0) {
                System.out.println("Angka Genap");
            } else {
                System.out.println("Bukan genap dan bukan 2");
            }
            // Percabangan cek kata
            if (length < 5) {
                System.out.println("Kata yang anda masukkan adalah " + kata);
                System.out.println("Panjang katanya kurang dari 5");
            } else if (length > 22) {
                System.out.println("Kata yang anda masukkan sangat panjang");
            } else {
                System.out.println("Kata yang anda masukkan biasa saja");
            }
        } catch (Exception InputMismatchException) {
            System.out.println("Masukkan input sesuai perintah!");
        }
    }
}
