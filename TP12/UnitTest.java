import static org.junit.Assert.*;
import java.io.File;
import java.io.FileNotFoundException;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;
 
public class UnitTest {
  @Test
  public void testKuliah01(){
      Mahasiswa mhs1 = new Mahasiswa("Burhan","129500004Y");
      Mahasiswa mhs2 = new Mahasiswa("Bubur","1299123456");
      MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");
 
      kul1.tambahMhs(mhs1);
      kul1.tambahMhs(mhs2);
 
      Mahasiswa[] daftarMhs = kul1.getDaftarMhs();
      List<Mahasiswa> arr = Arrays.asList(daftarMhs);
 
      assertEquals(kul1.getNama(), "DDP2");
      assertEquals(kul1.getCode(), "CSGE601021");
      assertTrue(arr.contains(mhs1));
      assertTrue(arr.contains(mhs2));
 
    }
}

