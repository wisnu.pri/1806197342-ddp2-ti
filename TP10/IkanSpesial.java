public class IkanSpesial extends Hewan implements Makhluk {
    private boolean isBeracun;

    public IkanSpesial(String nama, String spesies, boolean isBeracun){
        setNama(nama);
        setSpesies(spesies);
        this.isBeracun = isBeracun;
    }
    public String bersuara(){
        if(isBeracun == true){
            return "Blub blub blub blub. Blub. (Halo, saya " + getNama() + 
            ". Saya ikan yang beracun. Saya bisa terbang loh.";
        }else{
            return "Blub blub blub blub. Blub. (Halo, saya " + getNama() + 
            ". Saya ikan yang tidak beracun. Saya bisa terbang loh).";
        }
    }
    public String bernafas(){
        return getNama() + " bernafas dengan insang.";
    }
    public String bergerak(){
        return getNama() + " bergerak dengan cara berenang.";
    }
    public String terbang(){
        return "Fwooosssshhhhh! Plup.";
    }
    public String toString(){
        return bersuara();
    }
}
