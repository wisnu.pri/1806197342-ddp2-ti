public abstract class Manusia implements Makhluk
{
    private String nama;
    private int uang;
    private int umur;

    public Manusia(){
    }
    public Manusia(String nama, int uang, int umur){
        this.nama = nama;
        this.uang = uang;
        this.umur = umur;
    }
    public String getNama(){
        return nama;
    }
    public int getUang(){
        return uang;
    }
    public int getUmur(){
        return umur;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public void setUang(int uang){
        this.uang = uang;
    }
    public void setUmur(int umur){
        this.umur = umur;
    }
    public abstract String bicara();
    
}