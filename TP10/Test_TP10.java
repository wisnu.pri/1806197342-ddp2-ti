import java.util.Scanner;
import java.util.HashMap;

public class Test_TP10 {
    public static void main(String[] args){
        Scanner inp = new Scanner(System.in);
        HashMap<String, Object> obj = new HashMap<>();

        Manusia bujang = new Pegawai("Bujang", 100000, "pemula");
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        Hewan kerapu = new Ikan("Kerapu Batik", "Epinephelus Polyphekadion", false);
        Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);
        obj.put("bujang", bujang);
        obj.put("yoga", yoga);
        obj.put("kerapu", kerapu);
        obj.put("ikan terbang biru", ikanTerbang);

        boolean z = true;
        do {
            System.out.print("Silahkan masukkan perintah: ");
            String s = inp.nextLine().toLowerCase();
            if(!s.equals("selesai")){
                if(s.startsWith("panggil")){
                    if(obj.get(s.substring(s.lastIndexOf(" ")+1)) != null){
                        if(obj.get(s.substring(s.lastIndexOf(" ")+1)) instanceof Pegawai){
                            Pegawai o = (Pegawai) obj.get(s.substring(s.lastIndexOf(" ")+1));
                            System.out.println(o.toString());
                        }else if(obj.get(s.substring(s.lastIndexOf(" ")+1)) instanceof Pelanggan){
                            Pelanggan o = (Pelanggan) obj.get(s.substring(s.lastIndexOf(" ")+1));
                            System.out.println(o.toString());
                        }else if(obj.get(s.substring(s.lastIndexOf(" ")+1)) instanceof Ikan){
                            Ikan o = (Ikan) obj.get(s.substring(s.lastIndexOf(" ")+1));
                            System.out.println(o.toString());
                        }else if(obj.get(s.substring(s.lastIndexOf(" ")+1)) instanceof BurungHantu){
                            BurungHantu o = (BurungHantu) obj.get(s.substring(s.lastIndexOf(" ")+1));
                            System.out.println(o.toString());
                        }else if(obj.get(s.substring(s.lastIndexOf(" ")+1)) instanceof PegawaiSpesial){
                            PegawaiSpesial o = (PegawaiSpesial) obj.get(s.substring(s.lastIndexOf(" ")+1));
                            System.out.println(o.toString());
                        }else if(obj.get(s.substring(s.lastIndexOf(" ")+1)) instanceof IkanSpesial){
                            IkanSpesial o = (IkanSpesial) obj.get(s.substring(s.lastIndexOf(" ")+1));
                            System.out.println(o.toString());
                        }else{
                            System.out.println("Maaf, "+s.substring(s.lastIndexOf(" ")+1)+
                            " tidak pernah melewati/mampir di kedai.");
                        }
                    }else{
                        System.out.println("Maaf, "+s.substring(s.lastIndexOf(" ")+1)+
                        " tidak pernah melewati/mampir di kedai.");
                    }
                }else{
                    try{
                        if(obj.get(s.substring(0, s.lastIndexOf(" "))) != null){
                            if(obj.get(s.substring(0, s.lastIndexOf(" "))) instanceof Pegawai){
                                Pegawai o = (Pegawai) obj.get(s.substring(0, s.lastIndexOf(" ")));
                                switch(s.substring(s.lastIndexOf(" ")+1)){
                                    case "bergerak":
                                        System.out.println(o.bergerak()); break;
                                    case "bekerja":
                                        System.out.println(o.bekerja()); break;
                                    case "bernafas":
                                        System.out.println(o.bernafas()); break;
                                    case "bicara":
                                        System.out.println(o.bicara()); break;
                                    default:
                                        System.out.println("Maaf, Pegawai "+o.getNama()+
                                        " tidak bisa "+s.substring(s.lastIndexOf(" ")+1)+"."); break;
                                }
                            }else if(obj.get(s.substring(0, s.lastIndexOf(" "))) instanceof Pelanggan){
                                Pelanggan o = (Pelanggan) obj.get(s.substring(0, s.lastIndexOf(" ")));
                                switch(s.substring(s.lastIndexOf(" ")+1)){
                                    case "bergerak":
                                        System.out.println(o.bergerak()); break;
                                    case "beli":
                                        System.out.println(o.beli()); break;
                                    case "bernafas":
                                        System.out.println(o.bernafas()); break;
                                    case "bicara":
                                        System.out.println(o.bicara()); break;
                                    default:
                                        System.out.println("Maaf, Pelanggan "+o.getNama()+
                                        " tidak bisa "+s.substring(s.lastIndexOf(" ")+1)+"."); break;
                                }
                            }else if(obj.get(s.substring(0, s.lastIndexOf(" "))) instanceof Ikan){
                                Ikan o = (Ikan) obj.get(s.substring(0, s.lastIndexOf(" ")));
                                switch(s.substring(s.lastIndexOf(" ")+1)){
                                    case "bergerak":
                                        System.out.println(o.bergerak()); break;
                                    case "bernafas":
                                        System.out.println(o.bernafas()); break;
                                    case "bersuara":
                                        System.out.println(o.bersuara()); break;
                                    default:
                                        System.out.println("Maaf, Ikan "+o.getSpesies()+
                                        " tidak bisa "+s.substring(s.lastIndexOf(" ")+1)+"."); break;
                                }
                            }else if(obj.get(s.substring(0, s.lastIndexOf(" "))) instanceof BurungHantu){
                                BurungHantu o = (BurungHantu) obj.get(s.substring(0, s.lastIndexOf(" ")));
                                switch(s.substring(s.lastIndexOf(" ")+1)){
                                    case "bergerak":
                                        System.out.println(o.bergerak()); break;
                                    case "bernafas":
                                        System.out.println(o.bernafas()); break;
                                    case "bersuara":
                                        System.out.println(o.bersuara()); break;
                                    default:
                                        System.out.println("Maaf, Burung Hantu "+o.getSpesies()+
                                        " tidak bisa "+s.substring(s.lastIndexOf(" ")+1)+"."); break;
                                }
                            }else if(obj.get(s.substring(0, s.lastIndexOf(" "))) instanceof PegawaiSpesial){
                                PegawaiSpesial o = (PegawaiSpesial) obj.get(s.substring(0, s.lastIndexOf(" ")));
                                switch(s.substring(s.lastIndexOf(" ")+1)){
                                    case "bergerak":
                                        System.out.println(o.bergerak()); break;
                                    case "bekerja":
                                        System.out.println(o.bekerja()); break;
                                    case "bernafas":
                                        System.out.println(o.bernafas()); break;
                                    case "bicara":
                                        System.out.println(o.bicara()); break;
                                    case "libur":
                                        System.out.println(o.libur()); break;
                                    default:
                                        System.out.println("Maaf, Pegawai "+o.getNama()+
                                        " tidak bisa "+s.substring(s.lastIndexOf(" ")+1)+"."); break;
                                }
                            }else if(obj.get(s.substring(0, s.lastIndexOf(" "))) instanceof IkanSpesial){
                                IkanSpesial o = (IkanSpesial) obj.get(s.substring(0, s.lastIndexOf(" ")));
                                switch(s.substring(s.lastIndexOf(" ")+1)){
                                    case "bergerak":
                                        System.out.println(o.bergerak()); break;
                                    case "bernafas":
                                        System.out.println(o.bernafas()); break;
                                    case "bersuara":
                                        System.out.println(o.bersuara()); break;
                                    case "terbang":
                                        System.out.println(o.terbang()); break;
                                    default:
                                        System.out.println("Maaf, Ikan "+o.getSpesies()+
                                        " tidak bisa "+s.substring(s.lastIndexOf(" ")+1)+"."); break;
                                }
                            }else{
                                System.out.println("Maaf, perintah tidak ditemukan!");
                            }
                        }else{
                            System.out.println("Maaf, tidak ada yang bernama " + 
                            s.substring(0, s.lastIndexOf(" ")) + ".");
                        }
                    }catch(Exception e){
                        System.out.println("Maaf, perintah tidak dapat ditemukan!");
                    }
                }
            }else{
                z = false;
            }
        } while (z);
        System.out.println("Sampai jumpa!");
    }
}
