import static org.junit.Assert.*; 
import org.junit.Test;
import java.util.*;

    public class Junit {
      Double uang = 100000.0;

      @Test 
      public void testRequest(){
        Elektronik barangElektronik = new Elektronik("HP","Buruk");
        Elektronik barangElektronikKedua = new Elektronik("Modem","Menengah");
        Pakaian barangPakaian = new Pakaian("L".charAt(0),"Biru");
        Elektronik barangElektronikCSatu = new Elektronik("Laptop","Baru");
        Elektronik barangElektronikCDua = new Elektronik("Laptop","Baik");
        Pakaian barangPakaianCSatu = new Pakaian("S".charAt(0),"Merah");
        Pakaian barangPakaianCDua = new Pakaian("M".charAt(0),"Hijau");
        Double valueElektronik = 50.0;
        Double valuePakaian = 40.0;
        Double valueKardusElek = 130.0;
        ArrayList<Barang> objectElektronik = new ArrayList<Barang>();
        ArrayList<Barang> objectPakaian = new ArrayList<Barang>();
        ArrayList<Barang> objectCampuran = new ArrayList<Barang>();
        objectElektronik.add(barangElektronik);
        objectElektronik.add(barangElektronikKedua);
        objectPakaian.add(barangPakaian);
        objectCampuran.add(barangPakaianCSatu);
        objectCampuran.add(barangElektronikCSatu);
        objectCampuran.add(barangElektronikCDua);
        objectCampuran.add(barangPakaianCDua);
        Kardus<Barang> kardusElektronik = new Kardus<Barang>(objectElektronik);
        Kardus<Barang> kardusPakaian = new Kardus<Barang>(objectPakaian);
        Kardus<Barang> kardusCampuran = new Kardus<Barang>(objectCampuran);
        Double nilaiKardusElek = kardusElektronik.getTotalValue();
        assertEquals(barangElektronik.getValue(), valueElektronik);
        assertEquals(barangPakaian.getValue(), valuePakaian);
        assertEquals(nilaiKardusElek, valueKardusElek);
        assertEquals(kardusElektronik.rekap(), "Kardus Elektronik: Terdapat 2 barang elektronik dan 0 pakaian");
        assertEquals(kardusPakaian.rekap(), "Kardus Pakaian: Terdapat 0 barang elektronik dan 1 pakaian");
        assertEquals(kardusCampuran.rekap(), "Kardus Campuran: Terdapat 2 barang elektronik dan 2 pakaian");
        assertEquals(barangElektronik.getJenis(), "hp");
        assertEquals(barangElektronik.getKondisi(), "buruk");
        assertEquals(barangPakaian.getUkuran(), "L".charAt(0));
        assertEquals(barangPakaian.getWarna(), "biru".charAt(0));
        assertEquals(kardusElektronik.getListBarang(), objectElektronik);
        
        kardusElektronik.tambahBarang(barangElektronikCSatu);
        int testTambahBarang = kardusElektronik.getListBarang().size();
        int jumlahBarang = 3;
        assertEquals(testTambahBarang, jumlahBarang);
        
      }
}


