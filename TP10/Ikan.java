public class Ikan extends Hewan implements bisaTerbang {
    private boolean isBeracun;

    public Ikan(String nama, String spesies, boolean isBeracun){
        setNama(nama);
        setSpesies(spesies);
        this.isBeracun = isBeracun;
    }
    public String bersuara(){
        if(isBeracun == true){
            return "Blub blub blub blub. Blub. (Halo, saya " + getNama() + 
            ". Saya ikan yang beracun).";
        }else{
            return "Blub blub blub blub. Blub. (Halo, saya " + getNama() + 
            ". Saya ikan yang tidak beracun).";
        }
    }
    public String bernafas(){
        return getNama() + " bernafas dengan insang.";
    }
    public String bergerak(){
        return getNama() + " bergerak dengan cara berenang.";
    }
    public String toString(){
        return bersuara();
    }
}
